
MODS = 

all: start

start:
	mkdir -p log/ ebin/
	erlc -o ebin/ -Wall ${MODS:%=%.erl}
	sudo yaws --interactive --conf ./yaws.conf # -erlarg "-s db" # --id 37


up:
	erlc -o ./beams/ -Wall ${MODS:%=%.erl}
	sudo yaws --hup --id 37

stop:
	sudo yaws -stop --id 37

stats:
	sudo yaws --id 37 --running-config --status --stats --ls

clean:
	rm ./ebin/*.beam
	rm erl_crash.dump
