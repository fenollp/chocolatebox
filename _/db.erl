-module(db).

-include("records.hrl").
-include_lib("stdlib/include/qlc.hrl").
-define(Q(LisCompr),
  mnesia:transaction(fun() -> qlc:e(qlc:q(LisCompr)) end)).

-compile(export_all).
-export([get_lasts/1, save/5, start/0, get_by_id/1]).
-export([pre_fill/0, new_id/0, ls/0]).


start() ->
  mnesia:create_schema([node()]),
  mnesia:start(),
  I = mnesia:create_table(context, [{attributes, record_info(fields, context)},
    {type, ordered_set}, {disc_copies, [node()]}]),
  mnesia:wait_for_tables([context], 10000),
  pre_fill(),
  ls(),
  I.


new_id() ->
  case mnesia:transaction(fun () -> mnesia:read({context, 
    mnesia:last(context)}) end) of
  {aborted, _Raison} -> error;
  {atomic, [C]} -> C#context.id +1
  end.

ls() ->
  {atomic, C} = ?Q([ X || X <- mnesia:table(context) ]),
  io:format("~p\n", [C]).

pre_fill() ->
  mnesia:transaction(fun() -> mnesia:write( #context{id=0} ) end).


get_lasts(Count) ->
  Last_id = new_id(),  %% Last id +1 really
%  get_lasts(Last_id - Count, Last_id).
  get_all(Last_id, Count).
get_lasts(Tot, Tot) -> [];
get_lasts(N, Tot) ->
  case ?Q([ {X#context.id, X#context.title, X#context.url} ||
    X <- mnesia:table(context), X#context.id == N, X#context.private == no]) of
  {aborted, _} -> [ {none, none, none} | get_lasts(N +1, Tot) ];
  {atomic, Tuple} -> [ Tuple | get_lasts(N +1, Tot) ]
  end.

get_all(_, _) ->
  {atomic, C} = ?Q([X || X <- mnesia:table(context), X#context.private == no]),
  D = [ {X#context.id, X#context.title, X#context.url} || X <- C ],
  [ {Id, binary_to(T), binary_to(U)} || {Id, T, U} <- D ].


binary_to(none) -> none;
binary_to(A) -> binary_to_list(A).
binary_to_tupleofstrings([]) -> [];
binary_to_tupleofstrings([{Head, Er}|Headers]) ->
  [{binary_to_list(Head),binary_to_list(Er)}|binary_to_tupleofstrings(Headers)].

get_by_id(Id) ->
  Default = none, %{none, [], "http://127.0.0.1/?wtf"},
  case ?Q([ { binary_to(X#context.post_data), 
              binary_to_tupleofstrings(X#context.headers), 
              binary_to(X#context.url) } ||
    X <- mnesia:table(context), X#context.id == list_to_integer(Id) ]) of
  {atomic, [Tuple]} ->
    case Tuple of {_,_,_} -> Tuple; _ -> Default end;
  _Reason ->
    io:format("WARNING: smth weird happened in get_by_id/1: ~p", [[Id,_Reason]]),
    Default
  end.



to_binary(none) -> none;
to_binary(A) -> list_to_binary(A).

tupleofstrings_to_binary([]) -> [];
tupleofstrings_to_binary([{"", ""}|T]) ->
  [{<<"none">>, <<"none">>}| tupleofstrings_to_binary(T)];
tupleofstrings_to_binary([{"", B}|T]) ->
  [{<<"none">>, list_to_binary(B)}| tupleofstrings_to_binary(T)];
tupleofstrings_to_binary([{A, ""}|T]) ->
  [{list_to_binary(A), <<"none">>}| tupleofstrings_to_binary(T)];
tupleofstrings_to_binary([{Head, Er}|Headers]) ->
  [{list_to_binary(Head),list_to_binary(Er)}|tupleofstrings_to_binary(Headers)].


save(POST, Headers, Url, Status, Title) ->
  P = to_binary(POST),
  H = tupleofstrings_to_binary(Headers),
  U = to_binary(Url),
  T = to_binary(Title),
  Id = new_id(),
  Row = #context{id=Id, post_data=P, headers=H, url=U, private=Status, title=T},
  io:format("RECORD: ~p\n", [Row]),
  case mnesia:transaction(fun() -> mnesia:write(Row) end) of
  {aborted, Reason} -> {error, Reason};
  _ -> {ok, integer_to_list(Id)}
  end.


