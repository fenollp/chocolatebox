-record(context, {
  id            = none :: integer(),
  post_data     = none :: binary(),
  headers       = []   :: list(),
  url           = none :: binary(),
  private       = no   :: no | yes,
  title         = none :: binary()
}).
